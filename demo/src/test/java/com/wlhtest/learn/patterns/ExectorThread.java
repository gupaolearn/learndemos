package com.wlhtest.learn.patterns;

import com.wlhtest.learn.patterns.singleton.lazy.LazyDoubleCheckSingleton;

public class ExectorThread  implements  Runnable{


    @Override
    public void run() {
        /*LazySingleton  lazySingleton= LazySingleton.getInstance();*/

        LazyDoubleCheckSingleton lazySingleton=LazyDoubleCheckSingleton.getInstance();
        System.out.println(Thread.currentThread().getName()+":"+lazySingleton);
    }
}
