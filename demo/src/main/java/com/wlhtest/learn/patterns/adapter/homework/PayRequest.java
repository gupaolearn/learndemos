package com.wlhtest.learn.patterns.adapter.homework;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:30
 * @Version 1.0
 * @despricate:learn
 */
public class PayRequest {

    private  int payCode ;
    private  int money ;
    private  int orderId ;

    public int getPayCode() {
        return payCode;
    }

    public void setPayCode(int payCode) {
        this.payCode = payCode;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
