package com.wlhtest.learn.patterns.factory.AbstrFactory;

public class JeepCar implements  AbstrCar{

    @Override
    public void start() {
        System.out.println("start car");
    }
}
