package com.wlhtest.learn.patterns.template;

/**
 * @Author: wlh
 * @Date: 2019/3/19 14:52
 * @Version 1.0
 * @despricate:用户下单购买物品的流程
 */
public  abstract   class Buysomething {

    public  void  creteOrder(){

        //  1  浏览所有的商品  然后选择自己喜欢的
        this.scanGoods();
        
        //  2  订单加入购物车
        this.chooseGoods();

        // 3  创建订单
        this.creteMyOrder();

        // 4  选择支付方式 完成订单
        if(checkIsAvabl()){
            System.out.println("需要输入密码支付");
            this.payOrder();
        }else {
            System.out.println("直接免密支付完成订单");
        }
    }

    protected abstract Boolean checkIsAvabl();
        // 是否开通免密支付  如果是直接跳过
        //return  false ;


    protected abstract void payOrder();

    final void creteMyOrder(){
        System.out.println("系统创建订单");
    }

    protected abstract void chooseGoods();

    protected abstract  void scanGoods();
}
