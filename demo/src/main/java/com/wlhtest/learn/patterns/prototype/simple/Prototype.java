package com.wlhtest.learn.patterns.prototype.simple;

public interface Prototype {

    Prototype clone();
}
