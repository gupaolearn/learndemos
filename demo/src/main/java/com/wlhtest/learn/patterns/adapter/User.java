package com.wlhtest.learn.patterns.adapter;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:07
 * @Version 1.0
 * @despricate:learn
 */
public class User {
    private  String name ;
    private  String password ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
