package com.wlhtest.learn.patterns.proxy.Dymaic.WlhProxy;

import java.lang.reflect.Method;

public interface WlhInvokeactionHandler   {

    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable;
}
