package com.wlhtest.learn.patterns.proxy.Dymaic.jdkProxy;

import com.wlhtest.learn.patterns.proxy.Person;
import sun.misc.ProxyGenerator;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class JdkProxyTest {

    public static void main(String[] args) {

        Person person= null;
        try {
            person =(Person) new Meipo().getInstance(new Girl());
            System.out.println(person.getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 动态代理的原理：利用反射重新生成一个全新的类 ，调用jdk来完成编译  然后重写相关方法
        person.findLove();


        //  输出生成的类的源代码
        byte[] bytes= ProxyGenerator.generateProxyClass("$Proxy0",new  Class[]{Person.class});
        try {
            FileOutputStream fileOutputStream=new FileOutputStream("E://$Proxy0.class");
            fileOutputStream.write(bytes);
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
