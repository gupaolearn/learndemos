package com.wlhtest.learn.patterns.proxy.staticproxy.DBChoose;

/**
 * @Author: wlh
 * @Date: 2019/3/14 9:20
 * @Version 1.0
 * @despricate:learn
 */
public class OrderServiceImpl  implements  IOrderService  {

    private  OrderDao  orderDao ;

    public OrderServiceImpl(){
        // 实际spring 项目中会自动的注入
        orderDao=new OrderDao();
    }

    public void insertOrder(Order order) {
        System.out.println("服务层调用开始");
        orderDao.insertOrder(order);

    }
}
