package com.wlhtest.learn.patterns.template.jdbc;

import javax.sql.* ;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wlh
 * @Date: 2019/3/19 15:14
 * @Version 1.0
 * @despricate:learn
 */
public abstract class JdbcTemplate {

   // private  DateSources dateSources ;
    private DataSource dataSource ;

    public JdbcTemplate(DataSource dataSource){
        this.dataSource=dataSource;
    }

    public List<?> selectAll(String sql, RowMapper<?>  rowMapper,Object[] args) throws  Exception{
        //  1 创建连接
        Connection con=this.dataSource.getConnection() ;
        //  2  构造参数【先构建语句集，后拼入参数】
        PreparedStatement preparedStatement=con.prepareStatement(sql);
        if(args!=null && args.length>0){
            for (int i=0;i<args.length ;i++){
                preparedStatement.setObject(i,args[i]);
            }
        }
        //  3  执行语句
        ResultSet  resultSet=preparedStatement.executeQuery();
        //  4  对查询出来的数据进行处理 转换为对应的实体类
        List<?> res=null ;
        if(resultSet!=null) {
             res = paramResult(resultSet, rowMapper);
        }
        //  关闭对应的连接
        resultSet.close();
        preparedStatement.close();
        con.close();
        return  res ;
        
    }

    protected  List<?> paramResult(ResultSet resultSet, RowMapper<?> rowMapper) throws  Exception{
        List<Object>  result=new ArrayList<Object>();
        int rownum=1 ;
        while (resultSet.next()){
              result.add(rowMapper.rowToMap(resultSet,rownum++));
        }
       return   result ;

    }

}
