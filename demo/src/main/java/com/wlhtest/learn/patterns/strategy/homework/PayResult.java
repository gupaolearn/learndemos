/**
 * PayResult.java
 * com.tempus.gss.pay.entity.vo
 *
 * Function： TODO 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2018年1月16日 		lian.chen
 *
 * Copyright (c) 2018, TNT All Rights Reserved.
*/

package com.wlhtest.learn.patterns.strategy.homework;

import java.io.Serializable;

/**
 * ClassName:PayResult
 *
 * @author lian.chen
 * @version
 * @since Ver 1.1
 * @Date 2018年1月16日 下午5:22:19
 *
 * @see
 * 
 */
public class PayResult implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String SUCCESS = "success";

	public static final String FAIL = "fail";

	public static final String DOING = "doing";

	private String code;

	private String msg;

	private String url;

	public PayResult() {

		super();

	}

	public PayResult(String code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "PayResult [code=" + code + ", msg=" + msg + ", url=" + url + "]";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
