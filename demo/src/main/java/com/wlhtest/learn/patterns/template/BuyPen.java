package com.wlhtest.learn.patterns.template;

/**
 * @Author: wlh
 * @Date: 2019/3/19 15:06
 * @Version 1.0
 * @despricate:learn
 */
public class BuyPen extends  Buysomething {
    @Override
    protected Boolean checkIsAvabl() {
        return false;
    }

    @Override
    protected void payOrder() {
        System.out.println("支付铅笔订单");
    }

    @Override
    protected void chooseGoods() {
        System.out.println("选择铅笔类");
    }

    @Override
    protected void scanGoods() {
        System.out.println("浏览铅笔");
    }
}
