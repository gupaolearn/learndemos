package com.wlhtest.learn.patterns.prototype.simple;

import java.util.ArrayList;
import java.util.List;

public class PrototypeTest {

    public static void main(String[] args) {

        ConcreteProtoType concreteProtoType=new ConcreteProtoType() ;
        concreteProtoType.setAge(18);
        concreteProtoType.setName("tom");
        List<String> list=new ArrayList<String>();
        list.add("test");
        concreteProtoType.setHibbers(list);


        Client client=new Client();
        ConcreteProtoType concreteProtoType1=(ConcreteProtoType) client.startClone(concreteProtoType);
        System.out.println(concreteProtoType1);

        System.out.println("克隆对象中的引用类型的值："+concreteProtoType1.getHibbers());

        System.out.println("原对象中的引用类型的值："+concreteProtoType.getHibbers());

        System.out.println("对象地址的比较："+(concreteProtoType.getHibbers()==concreteProtoType1.getHibbers()));

        // 尝试修改其中一个对象的值   另外一个对象的值也会改变  如下：
        list.add("4734748");
        concreteProtoType.setHibbers(list);
        System.out.println(concreteProtoType1.getHibbers());

        // 总结：浅克隆只是完整的复制了值类型的数据，而没有赋值引用对象，所有的克隆对象都指向了原来的对象。
    }
}
