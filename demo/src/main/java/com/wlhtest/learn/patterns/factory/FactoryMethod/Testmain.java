package com.wlhtest.learn.patterns.factory.FactoryMethod;

/**
 * @Author: wlh
 * @Date: 2019/3/7 9:54
 * @Version 1.0
 * @despricate:learn
 */
public class Testmain {

    public static void main(String[] args) {
        IFactory factory=new JeepFactory();
        factory.createCar().start();

        IFactory factory1=new DazhongFactory();
        factory1.createCar().start();
    }
}
