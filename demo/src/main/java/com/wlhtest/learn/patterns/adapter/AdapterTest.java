package com.wlhtest.learn.patterns.adapter;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:13
 * @Version 1.0
 * @despricate:learn
 */
public class AdapterTest {

    public static void main(String[] args) {
        // 原有的登录方法
        LoginService loginService=new LoginService() ;
        System.out.println(loginService.login("admin","123456").getMessage());

        // 适配器的登录
        LoginForThreedWay loginForThreedWay=new LoginForThreedWay() ;
        System.out.println(loginForThreedWay.loginByAli("AA323783373278").getMessage());
        System.out.println(loginForThreedWay.loginByQQ("QQ","123323893","123456").getMessage());


    }


}
