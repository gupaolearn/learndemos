package com.wlhtest.learn.patterns.delegate.mvc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class DispatchServlet  extends HttpServlet {

    private List<handler> handlerMappinig=new ArrayList<handler>();

    @Override
    public void init() throws ServletException {
        // 完成url 与对应congtroller 对应关系的初始化
        //super.init();
       try {
           Class<?> orderController = OrderController.class;
           handlerMappinig.add(new handler().setController(orderController.getName()).
                   setMethod(orderController.getMethod("getByOrderIsd", new Class[]{String.class}))
                   .setUrl("/web/getByOrderIsd.do"));

           Class<?> membertroller = OrderController.class;
           handlerMappinig.add(new handler().setController(membertroller.getName()).
                   setMethod(orderController.getMethod("getByOrderIsd", new Class[]{String.class}))
                   .setUrl("/web/getMemberById.do"));
       }catch (Exception e){
           e.printStackTrace();
       }

    }


    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.service(req, resp);
        doMyDispatchServlet(req,resp);
    }

    private void doMyDispatchServlet(HttpServletRequest req, HttpServletResponse resp) {

        String url=req.getRequestURI() ;
        handler handler=null ;

        //  实际上此处就是采用了委派模式  或者说策略模式
        for (handler h:handlerMappinig){
            if(url.equals(h.getUrl()))
            {
                handler= h;
                break;
            }
        }

        try {
          Object object=  handler.getMethod().invoke(handler.getController(),req.getParameter("id"));
            try {
                resp.getWriter().write(object.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }




    class  handler{
        private  Object controller ;
        private Method method ;
        private  String url ;


        //

        public Object getController() {
            return controller;
        }

        public handler setController(Object controller) {
            this.controller = controller;
            return  this ;
        }

        public Method getMethod() {
            return method;
        }

        public handler setMethod(Method method) {
            this.method = method;
            return  this ;
        }

        public String getUrl() {
            return url;
        }

        public handler setUrl(String url) {
            this.url = url;
            return  this ;
        }
    }


}
