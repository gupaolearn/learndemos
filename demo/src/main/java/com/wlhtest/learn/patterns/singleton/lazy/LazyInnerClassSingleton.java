package com.wlhtest.learn.patterns.singleton.lazy;



public class LazyInnerClassSingleton {


    // 没有使用synci...ed    也避免了线程之间的安全问题
    private  LazyInnerClassSingleton(){

    }

    public static  final  LazyInnerClassSingleton getInstance(){

        return  LazyHolder.lazy ;
    }

    private  static  class  LazyHolder{
        private static  final  LazyInnerClassSingleton lazy=new LazyInnerClassSingleton();
    }
}
