package com.wlhtest.learn.patterns.proxy.staticproxy.DBChoose;

/**
 * @Author: wlh
 * @Date: 2019/3/14 9:20
 * @Version 1.0
 * @despricate:learn
 */
public interface IOrderService {

    void  insertOrder(Order order);
}
