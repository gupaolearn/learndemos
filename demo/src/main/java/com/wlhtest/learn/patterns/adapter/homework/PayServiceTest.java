package com.wlhtest.learn.patterns.adapter.homework;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:35
 * @Version 1.0
 * @despricate:learn
 */
public class PayServiceTest {

    public static void main(String[] args) {
        Alipay alipay=new Alipay();
        alipay.doPay(new PayRequest());

        PayServiceAdapter payServiceAdapter=new PayServiceAdapter();
        payServiceAdapter.doPayByNo(new PayRequest());
        payServiceAdapter.doPayByOther(new PayRequest());

    }
}
