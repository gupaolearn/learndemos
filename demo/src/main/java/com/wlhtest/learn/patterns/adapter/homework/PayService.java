package com.wlhtest.learn.patterns.adapter.homework;

import com.wlhtest.learn.patterns.adapter.MsgResult;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:28
 * @Version 1.0
 * @despricate: 支付接口 原本只支持指定方式下的余额支付    先需要扩展为支持点卷、代付
 */
public interface PayService {

    MsgResult doPay(PayRequest payRequest);
}
