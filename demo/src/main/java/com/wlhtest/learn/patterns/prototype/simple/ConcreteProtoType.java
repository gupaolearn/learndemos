package com.wlhtest.learn.patterns.prototype.simple;

import java.util.List;

public class ConcreteProtoType  implements  Prototype {

    private  String name ;

    private  int age ;

    public List hibbers ;

    public List getHibbers() {
        return hibbers;
    }

    public void setHibbers(List hibbers) {
        this.hibbers = hibbers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Prototype clone() {


        ConcreteProtoType concreteProtoType=new ConcreteProtoType() ;
        concreteProtoType.setAge(this.age);
        concreteProtoType.setName(this.name);
        concreteProtoType.setHibbers(this.hibbers);
        return  concreteProtoType ;
    }
}
