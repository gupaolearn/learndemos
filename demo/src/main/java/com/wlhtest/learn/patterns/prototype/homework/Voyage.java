package com.wlhtest.learn.patterns.prototype.homework;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 * <b>航程信息.</b>
 * <b>Description:</b> 
 *    
 * <b>Author:</b> cz
 * <b>Date:</b> 2016年9月8日 上午8:41:36
 * <b>Copyright:</b> Copyright ©2016 tempus.cn. All rights reserved.
 * <b>Changelog:</b>
 *   Ver   Date                    Author                Detail
 *   ----------------------------------------------------------------------
 *   0.1   2016年9月8日 上午8:41:36    cz
 *         new file.
 * </pre>
 */
public class Voyage implements  Cloneable,Serializable{

	/** 日期 */
	protected Date times;

	/** 航司 */
	protected String airline;

	/** 儿童舱位 */
	protected String chdCabin;

	/** 婴儿舱位 */
	protected String infCabin;

	/** 状态, 0:取消; 1:正常(默认) */
	protected Integer status;


	@Override
	protected Object clone() throws CloneNotSupportedException {
		return deepClone();
	}

	public  Object deepClone(){
    try {
	Voyage voyate=(Voyage)super.clone();
	voyate.times=new Date();
	return  voyate ;
     }catch (Exception e){
	e.printStackTrace();
   }
	return  null ;

	}
}
