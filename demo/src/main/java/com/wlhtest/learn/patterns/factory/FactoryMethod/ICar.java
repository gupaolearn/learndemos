package com.wlhtest.learn.patterns.factory.FactoryMethod;

/**
 * @Author: wlh
 * @Date: 2019/3/7 9:43
 * @Version 1.0
 * @despricate:learn
 */
public interface ICar {

    public  void   start();
}
