package com.wlhtest.learn.patterns.proxy.staticproxy.DBChoose;

import java.util.Calendar;
import java.util.Date;

/**
 * @Author: wlh
 * @Date: 2019/3/14 9:50
 * @Version 1.0
 * @despricate:learn
 */
public class ProxyTest {

    public static void main(String[] args) {

         Order order=new Order() ;
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());


         for (int i=0 ;i<20;i++){
             c.add(Calendar.YEAR, -i);
             order.setCreateTime(c.getTime());
             IOrderService orderService=new OrderServiceStaticProxy();
             orderService.insertOrder(order);
             System.out.println("---------------------------------------------");
         }
    }

}
