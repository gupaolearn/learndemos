package com.wlhtest.learn.patterns.proxy.staticproxy.DBChoose;

import java.util.Date;

/**
 * @Author: wlh
 * @Date: 2019/3/14 9:32
 * @Version 1.0
 * @despricate:learn
 */
public class OrderServiceStaticProxy  implements  IOrderService {

    private  IOrderService orderService ;

    public  OrderServiceStaticProxy(){
        orderService=new OrderServiceImpl();
    }

    public void insertOrder(Order order) {

        //  插入数据前 可能存在一些数据的校验  及其他默认属性的设置  此处直接省去
        before();

        Date time=order.getCreateTime() ;
        System.out.println("分配数据到DB_"+time.getYear());
        DynamicDataSourcesEntity.setByName("DB_"+time.getYear());
        orderService.insertOrder(order);
        //   订单数据插入完成后，可能后续的还有一些其他的处理
        after();


    }


    private void before() {
        System.out.println("执行数据的校验");
    }

    private void after() {
        System.out.println("执行数据的后续处理");
    }
}
