package com.wlhtest.learn.patterns.factory.simpleFactory;

public class TestDemo {

    public static void main(String[] args) {

        SimpleFactory simpleFactory=new SimpleFactory() ;
        ICoures coures=simpleFactory.createCoures("java");
        coures.study();

        ICoures coures1=simpleFactory.createCoures("ttt");
        coures1.study();

    }
}
