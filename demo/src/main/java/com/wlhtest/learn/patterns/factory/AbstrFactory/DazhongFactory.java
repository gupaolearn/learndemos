package com.wlhtest.learn.patterns.factory.AbstrFactory;

public class DazhongFactory implements   AbstrFactory{


    @Override
    public AbstrCar createCar() {
        return null;
    }

    @Override
    public AbstrDoor createDoor() {
        return new GeliDoor();
    }
}
