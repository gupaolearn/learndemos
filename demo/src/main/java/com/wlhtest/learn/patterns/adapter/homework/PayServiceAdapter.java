package com.wlhtest.learn.patterns.adapter.homework;

import com.wlhtest.learn.patterns.adapter.MsgResult;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:32
 * @Version 1.0
 * @despricate:learn
 */
public class PayServiceAdapter implements PayService {
    public MsgResult doPay(PayRequest payRequest) {
        return null;
    }

    public MsgResult doPayByNo(PayRequest payRequest) {
        System.out.println("通过点卷支付");
        return new MsgResult(1,"success",null);
    }

    public MsgResult doPayByOther(PayRequest payRequest) {
        System.out.println("通过他人支付");
        return new MsgResult(1,"success",null);
    }

}
