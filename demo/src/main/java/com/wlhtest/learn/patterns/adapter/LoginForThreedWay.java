package com.wlhtest.learn.patterns.adapter;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:08
 * @Version 1.0
 * @despricate:learn
 */
public class LoginForThreedWay  extends  LoginService {

    public MsgResult loginByQQ(String type,String username, String password) {
        // 调用第三方qq登录接口  并接收器返回结果
        MsgResult msgResult=new MsgResult(0,"failure",null);
        return  msgResult ;
    }

    public MsgResult loginByAli(String  openId) {
        // 调用第三方阿里的登录接口  并接收器返回结果
       MsgResult msgResult=new MsgResult(1,"success",null);
       return  msgResult ;
    }


}
