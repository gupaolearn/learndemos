package com.wlhtest.learn.patterns.factory.AbstrFactory;

public class GeliFactory implements   AbstrFactory {


    @Override
    public AbstrCar createCar() {
        return new JeepCar();
    }

    @Override
    public AbstrDoor createDoor() {
        return null;
    }
}
