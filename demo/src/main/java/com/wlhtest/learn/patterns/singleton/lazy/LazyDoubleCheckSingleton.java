package com.wlhtest.learn.patterns.singleton.lazy;

public class LazyDoubleCheckSingleton {

    private  volatile  static LazyDoubleCheckSingleton lazy=null;
    private LazyDoubleCheckSingleton(){

    }


    // 适中的方法  使得线程能进入方法  类不至于被锁
    public    static LazyDoubleCheckSingleton getInstance(){
        if (lazy==null){
            synchronized(LazyDoubleCheckSingleton.class){
                //  防止其他线程已经实例化  避免多次实例化
                if (lazy==null){
                    lazy=new LazyDoubleCheckSingleton() ;
                    /*
                    * cpu 执行程序的时候转换为jvm指令执行  其中 2  3的执行顺序不确定 （指令重排序）
                    * 1  分配内存给这个对象
                    * 2  初始化对象
                    * 3  将初始化好的对象和地址建立关联、赋值
                    * 4  用户初次访问
                    *
                    *
                    * */

                }
            }
        }
        return  lazy;
    }
}
