package com.wlhtest.learn.patterns.proxy;

import com.wlhtest.learn.patterns.proxy.staticproxy.Father;
import com.wlhtest.learn.patterns.proxy.staticproxy.Son;

/**
 * @Author: wlh
 * @Date: 2019/3/14 9:13
 * @Version 1.0
 * @despricate:learn
 */
public class ProxyTest {

    public static void main(String[] args) {
           Father father=new Father(new Son()) ;
           father.findLove();
    }
}
