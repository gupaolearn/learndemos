package com.wlhtest.learn.patterns.proxy.Dymaic.cglibProxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibMeipo  implements MethodInterceptor {

    public  Object  getInstance(Class<?> clazz) throws  Exception{

        Enhancer enhancer=new Enhancer() ;
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(this);
        return  enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
      before();
      Object o1=methodProxy.invokeSuper(o,objects);

      after();
        
        return o1;
    }

    private Object after() {
        return  null ;
    }

    private Object before() {
        return  null ;
    }
}
