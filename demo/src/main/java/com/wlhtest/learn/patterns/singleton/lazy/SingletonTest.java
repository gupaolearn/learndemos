package main.java.com.wlhtest.learn.patterns.singleton.lazy;

import java.lang.reflect.Constructor;

/**
 * @Author: wlh
 * @Date: 2019/4/11 10:37
 * @Version 1.0
 * @despricate:learn
 */
public class SingletonTest {

    //  单例的测试

    public static void main(String[] args) {
        try{

            // 使用反射 进行单例模式的破坏
            Class<?> clazz= com.wlhtest.learn.patterns.singleton.lazy.LazySingleton.class ;

            Constructor  c=clazz.getDeclaredConstructor(null);

            c.setAccessible(true);

            Object o1=c.newInstance();

            Object o2=c.newInstance();

            System.out.println(o1==o2);

        }catch (Exception exception){

        }

    }
}
