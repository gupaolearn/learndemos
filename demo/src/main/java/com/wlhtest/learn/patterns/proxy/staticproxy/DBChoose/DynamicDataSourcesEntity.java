package com.wlhtest.learn.patterns.proxy.staticproxy.DBChoose;

/**
 * @Author: wlh
 * @Date: 2019/3/14 9:23
 * @Version 1.0
 * @despricate: 动态数据代理，自动根据数据的创建时间 分发到不同的数据库中
 */
public   class DynamicDataSourcesEntity {

    public    final static String  default_Sources=null ;

    private  final  static  ThreadLocal<String>  local=new ThreadLocal<String>();


    // 私有化构造方法
    private DynamicDataSourcesEntity(){

    }

    // 清空数据源
    public  void  clear(){
        local.remove();
    }

    //  获取当前正在使用的数据源名称
    public  String getName(){
        return  local.get();
    }

    // 还原当前切面的数据源
    public  void  reSet(){
        local.set(default_Sources);
    }

   //  设置已知的数据源
    public static void  setByName(String dataSources){
        local.set(dataSources);
    }

    // 根据年份动态设置数据源
    public void setByYear(int year){
        local.set("DB_"+year);
    }




}
