package com.wlhtest.learn.patterns.prototype.homework;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 *
 * 所有交易流水
 *
 */
public class AllPayInfo implements Serializable {


	protected static final long serialVersionUID = 1L;

	/** 记录ID */

	protected Long id;

	/** 业务编号 */
	protected Long allPayInfoNo;

	/** 客户业务编号 */
	protected Long custUserNo;
	
	/** 客户名称 */
	protected String custUserName;

	/** 客户账号业务编号 **/
	protected Long custUserAccountNo;
	
	/** 客户账号名称 **/
	//@TableField(exist = false)
	//protected String custUserAccountName;

	/** 渠道 */
	protected String custChannel;

	/** 业务线 单个，如1,2(支付订单、充值) */
	protected String serviceLine;

	/** 订单号 充值时订单号固定为0 */
	protected Long orderNo;

	/** 订单类型 1主订单 2充值 */
	protected Integer orderType;

	/** 类别代码 */
	protected Integer payCategoryCode;

	/** 类别名称 */
	protected String payCategoryName;

	/** 方式代码 */
	protected Integer payWayCode;

	/** 方式名称 */
	protected String payWayName;

	/** 第三方对接设置 */
	protected Long threeInterfaceSetNo;

	/** 交易金额 */
	protected BigDecimal amount;

	/** 实际交易金额 */
	protected BigDecimal realAmount;

	/** 平台费率 */
	protected BigDecimal platRate;

	/** 第三方费率 */
	protected BigDecimal threeRate;

	/** 平台交易费用 */
	protected BigDecimal platFare;

	/** 第三方交易费用 */
	protected BigDecimal threeFare;

	/** 外部交易流水号 如支付宝交易流水号 */
	protected String threeTradeNo;

	/** 原交易流水号 退款或分账时使用 */
	protected String oldAllPayInfoNo;

	/** 原外部交易流水号 退款或分账时使用 */
	protected String oldThreeTradeNo;

	/** 交易类型 1充值 2支付 3退款  4分账 */
	protected Integer tradeType;

	/** 交易状态 0初始化 1成功 2失败 */
	protected Integer tradeStatus;

	/** 审核状态 0无需审核 1待审核 2审核通过 3审核拒绝 */
	protected Integer auditStatus;

	/** 传入到第三方的标识 */
	protected String mark;

	/** 支付提交信息 */
	protected String submitInfo;

	/** 支付返回信息 */
	protected String calbackInfo;

	/** 付款方账号 */
	protected String paymentAccount;

	/** 付款方账号名 */
	protected String paymentAccountName;

	/** 收款方账号 */
	protected String receivableAccount;

	/** 收款方账号名 */
	protected String receivableAccountName;

	/** 交易前额度 针对额度账户 */
	protected BigDecimal beforeBalance;

	/** 交易后额度 针对额度账户 */
	protected BigDecimal afterBalance;

	/** 客户备注 */
	protected String custRemark;

	/** 审核备注 */
	protected String auditRemark;

	/** 交易IP */
	protected String paymentIp;

	/** 交易地址 */
	protected String paymentAddress;
	
	/** 交易时间 */
	protected Date paymentTime;

	/** 创建人 */
	protected String creator;

	/** 创建时间 */
	protected Date createTime;

	/** 审核人 */
	protected String auditor;

	/** 审核时间 */
	protected Date auditTime;

	/** 完成人 */
	protected String completer;

	/** 完成时间 */
	protected Date completeTime;

	/** 数据归属单位 */
	protected Integer owner;

	/** 经办人 **/
	private String operator;
	
	/** 经办人电话 */
	private String operatorTel;

	/** 收银成功后回调地址 **/
	private String callBackUrl;
	
	/** 交易单号 */
	private String transationOrderNo;

	/** 销售单号 */
	private String saleOrderNo;

	/** pnr */
	private String pnr;

	/** 第三方回传通知时间 */
	private Date notifyTime;
	
	/** 交易凭证地址 */
	private String transationCertificateUrl;
	
	/** 交易类型 如xx银行，支付宝 */
	private String paymentType;
	
	/** 还款金额 */
	protected BigDecimal paybackAmount;
	
	/** 到账时间 */
	protected Date receiveTime;
	
	/** 支付的产品类型 */
	private Integer productType;
	
	/** 支付方式 */
	private Integer payWay;
	
	/** 充值方式 ：1：银行转账      2：支票存款      3：现金存款 */
	private Integer rechargePayType ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAllPayInfoNo() {
		return allPayInfoNo;
	}

	public void setAllPayInfoNo(Long allPayInfoNo) {
		this.allPayInfoNo = allPayInfoNo;
	}

	public Long getCustUserNo() {
		return custUserNo;
	}

	public void setCustUserNo(Long custUserNo) {
		this.custUserNo = custUserNo;
	}

	public String getCustUserName() {
		return custUserName;
	}

	public void setCustUserName(String custUserName) {
		this.custUserName = custUserName;
	}

	public Long getCustUserAccountNo() {
		return custUserAccountNo;
	}

	public void setCustUserAccountNo(Long custUserAccountNo) {
		this.custUserAccountNo = custUserAccountNo;
	}

	public String getCustChannel() {
		return custChannel;
	}

	public void setCustChannel(String custChannel) {
		this.custChannel = custChannel;
	}

	public String getServiceLine() {
		return serviceLine;
	}

	public void setServiceLine(String serviceLine) {
		this.serviceLine = serviceLine;
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public Integer getPayCategoryCode() {
		return payCategoryCode;
	}

	public void setPayCategoryCode(Integer payCategoryCode) {
		this.payCategoryCode = payCategoryCode;
	}

	public String getPayCategoryName() {
		return payCategoryName;
	}

	public void setPayCategoryName(String payCategoryName) {
		this.payCategoryName = payCategoryName;
	}

	public Integer getPayWayCode() {
		return payWayCode;
	}

	public void setPayWayCode(Integer payWayCode) {
		this.payWayCode = payWayCode;
	}

	public String getPayWayName() {
		return payWayName;
	}

	public void setPayWayName(String payWayName) {
		this.payWayName = payWayName;
	}

	public Long getThreeInterfaceSetNo() {
		return threeInterfaceSetNo;
	}

	public void setThreeInterfaceSetNo(Long threeInterfaceSetNo) {
		this.threeInterfaceSetNo = threeInterfaceSetNo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getRealAmount() {
		return realAmount;
	}

	public void setRealAmount(BigDecimal realAmount) {
		this.realAmount = realAmount;
	}

	public BigDecimal getPlatRate() {
		return platRate;
	}

	public void setPlatRate(BigDecimal platRate) {
		this.platRate = platRate;
	}

	public BigDecimal getThreeRate() {
		return threeRate;
	}

	public void setThreeRate(BigDecimal threeRate) {
		this.threeRate = threeRate;
	}

	public BigDecimal getPlatFare() {
		return platFare;
	}

	public void setPlatFare(BigDecimal platFare) {
		this.platFare = platFare;
	}

	public BigDecimal getThreeFare() {
		return threeFare;
	}

	public void setThreeFare(BigDecimal threeFare) {
		this.threeFare = threeFare;
	}

	public String getThreeTradeNo() {
		return threeTradeNo;
	}

	public void setThreeTradeNo(String threeTradeNo) {
		this.threeTradeNo = threeTradeNo;
	}

	public String getOldAllPayInfoNo() {
		return oldAllPayInfoNo;
	}

	public void setOldAllPayInfoNo(String oldAllPayInfoNo) {
		this.oldAllPayInfoNo = oldAllPayInfoNo;
	}

	public String getOldThreeTradeNo() {
		return oldThreeTradeNo;
	}

	public void setOldThreeTradeNo(String oldThreeTradeNo) {
		this.oldThreeTradeNo = oldThreeTradeNo;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Integer getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getSubmitInfo() {
		return submitInfo;
	}

	public void setSubmitInfo(String submitInfo) {
		this.submitInfo = submitInfo;
	}

	public String getCalbackInfo() {
		return calbackInfo;
	}

	public void setCalbackInfo(String calbackInfo) {
		this.calbackInfo = calbackInfo;
	}

	public String getPaymentAccount() {
		return paymentAccount;
	}

	public void setPaymentAccount(String paymentAccount) {
		this.paymentAccount = paymentAccount;
	}

	public String getPaymentAccountName() {
		return paymentAccountName;
	}

	public void setPaymentAccountName(String paymentAccountName) {
		this.paymentAccountName = paymentAccountName;
	}

	public String getReceivableAccount() {
		return receivableAccount;
	}

	public void setReceivableAccount(String receivableAccount) {
		this.receivableAccount = receivableAccount;
	}

	public String getReceivableAccountName() {
		return receivableAccountName;
	}

	public void setReceivableAccountName(String receivableAccountName) {
		this.receivableAccountName = receivableAccountName;
	}

	public BigDecimal getBeforeBalance() {
		return beforeBalance;
	}

	public void setBeforeBalance(BigDecimal beforeBalance) {
		this.beforeBalance = beforeBalance;
	}

	public BigDecimal getAfterBalance() {
		return afterBalance;
	}

	public void setAfterBalance(BigDecimal afterBalance) {
		this.afterBalance = afterBalance;
	}

	public String getCustRemark() {
		return custRemark;
	}

	public void setCustRemark(String custRemark) {
		this.custRemark = custRemark;
	}

	public String getAuditRemark() {
		return auditRemark;
	}

	public void setAuditRemark(String auditRemark) {
		this.auditRemark = auditRemark;
	}

	public String getPaymentIp() {
		return paymentIp;
	}

	public void setPaymentIp(String paymentIp) {
		this.paymentIp = paymentIp;
	}

	public String getPaymentAddress() {
		return paymentAddress;
	}

	public void setPaymentAddress(String paymentAddress) {
		this.paymentAddress = paymentAddress;
	}

	public Date getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAuditor() {
		return auditor;
	}

	public void setAuditor(String auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getCompleter() {
		return completer;
	}

	public void setCompleter(String completer) {
		this.completer = completer;
	}

	public Date getCompleteTime() {
		return completeTime;
	}

	public void setCompleteTime(Date completeTime) {
		this.completeTime = completeTime;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperatorTel() {
		return operatorTel;
	}

	public void setOperatorTel(String operatorTel) {
		this.operatorTel = operatorTel;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getTransationOrderNo() {
		return transationOrderNo;
	}

	public void setTransationOrderNo(String transationOrderNo) {
		this.transationOrderNo = transationOrderNo;
	}

	public String getSaleOrderNo() {
		return saleOrderNo;
	}

	public void setSaleOrderNo(String saleOrderNo) {
		this.saleOrderNo = saleOrderNo;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Date getNotifyTime() {
		return notifyTime;
	}

	public void setNotifyTime(Date notifyTime) {
		this.notifyTime = notifyTime;
	}

	public String getTransationCertificateUrl() {
		return transationCertificateUrl;
	}

	public void setTransationCertificateUrl(String transationCertificateUrl) {
		this.transationCertificateUrl = transationCertificateUrl;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public BigDecimal getPaybackAmount() {
		return paybackAmount;
	}

	public void setPaybackAmount(BigDecimal paybackAmount) {
		this.paybackAmount = paybackAmount;
	}

	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getPayWay() {
		return payWay;
	}

	public void setPayWay(Integer payWay) {
		this.payWay = payWay;
	}

	public Integer getRechargePayType() {
		return rechargePayType;
	}

	public void setRechargePayType(Integer rechargePayType) {
		this.rechargePayType = rechargePayType;
	}
}
