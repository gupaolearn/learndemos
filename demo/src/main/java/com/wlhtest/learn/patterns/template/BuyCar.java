package com.wlhtest.learn.patterns.template;

/**
 * @Author: wlh
 * @Date: 2019/3/19 15:00
 * @Version 1.0
 * @despricate:learn
 */
public class BuyCar  extends  Buysomething {

    @Override
    protected Boolean checkIsAvabl() {
        return true;
    }

    @Override
    protected void payOrder() {
        System.out.println("支付汽车购买记录");
    }

    @Override
    protected void chooseGoods() {
        System.out.println("只能选择汽车品牌");
    }

    @Override
    protected void scanGoods() {
        System.out.println("只能浏览汽车类");
    }
}
