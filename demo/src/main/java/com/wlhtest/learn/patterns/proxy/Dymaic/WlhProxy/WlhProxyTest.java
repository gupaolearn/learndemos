package com.wlhtest.learn.patterns.proxy.Dymaic.WlhProxy;

import com.wlhtest.learn.patterns.proxy.Dymaic.jdkProxy.Girl;
import com.wlhtest.learn.patterns.proxy.Person;

public class WlhProxyTest {

    public static void main(String[] args) {

        Person person= null;
        try {
            person =(Person) new WlhMeipo().getInstance(new Girl());
            System.out.println(person.getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 动态代理的原理：利用反射重新生成一个全新的类 ，调用jdk来完成编译  然后重写相关方法  重新的方法都是final类型
        //  也就是防止被重新覆盖   jdk 动态代理的类 必须实现有关接口  否则就不能根据反射生成相应的接口
        person.findLove();


        //  输出生成的类的源代码
       /* byte[] bytes= ProxyGenerator.generateProxyClass("$Proxy0",new  Class[]{Person.class});
        try {
            FileOutputStream fileOutputStream=new FileOutputStream("E://$Proxy0.class");
            fileOutputStream.write(bytes);
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/


    }
}
