package com.wlhtest.learn.patterns.adapter;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:04
 * @Version 1.0
 * @despricate:全局的执行结果类
 */
public class MsgResult {

    private  int code ;

    private  String message ;

    private  Object data ;

    public MsgResult(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
