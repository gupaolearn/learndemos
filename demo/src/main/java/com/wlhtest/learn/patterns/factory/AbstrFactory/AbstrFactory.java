package com.wlhtest.learn.patterns.factory.AbstrFactory;

/**
 * @Author: wlh
 * @Date: 2019/3/8 17:29
 * @Version 1.0
 * @despricate:learn
 */
public  interface AbstrFactory {

        AbstrCar createCar();

        AbstrDoor createDoor();


}
