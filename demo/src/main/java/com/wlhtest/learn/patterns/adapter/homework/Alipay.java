package com.wlhtest.learn.patterns.adapter.homework;

import com.wlhtest.learn.patterns.adapter.MsgResult;

/**
 * @Author: wlh
 * @Date: 2019/3/19 17:31
 * @Version 1.0
 * @despricate:learn
 */
public class Alipay implements  PayService {
    public MsgResult doPay(PayRequest payRequest) {
        System.out.println("正常支付");
        return null;
    }
}
