package com.wlhtest.learn.patterns.prototype.homework;

import com.wlhtest.learn.patterns.prototype.homework.Voyage;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

/**
 * @Author: wlh
 * @Date: 2019/3/13 14:48
 * @Version 1.0
 * @despricate:learn
 */
public class HomeWorkTest {


    public static void main(String[] args) {

     OnLinePayRequest onLinePayRequest=new OnLinePayRequest();
     AllPayInfo allPayInfo=coloneAllpayinfo(onLinePayRequest);
        System.out.println(allPayInfo);

    }

    public static   AllPayInfo coloneAllpayinfo(OnLinePayRequest request){
        AllPayInfo allPayInfo = new AllPayInfo();
        try {
            BeanUtils.copyProperties(allPayInfo, request);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return  allPayInfo ;
    }




    /**
     * buildPayInfo:构建交易流水对象     未使用原型模式之前的代码
     * @param request
     * @return 如果订单已完成返回null
     */
    private AllPayInfo buildPayInfo(OnLinePayRequest request) {

        // 判断订单是否存在
        AllPayInfo allPayInfo = new AllPayInfo();

        // 流水不存在或者交易状态是失败或者未支付则生成新的
        allPayInfo = new AllPayInfo();

        allPayInfo.setCustUserNo(request.getCusId());
        allPayInfo.setCustChannel(request.getChannel());
        allPayInfo.setServiceLine("1");
        allPayInfo.setOrderNo(request.getOrderNo());
        allPayInfo.setOrderType(1);

        allPayInfo.setAmount(request.getBalance());
        allPayInfo.setPlatRate(BigDecimal.valueOf(0));// 平台费率
        allPayInfo.setPlatFare(BigDecimal.valueOf(0)); // 平台交易费用
        allPayInfo.setCustRemark(request.getCustRemark());
        allPayInfo.setTradeType(2);//// 交易类型1充值2支付3退款
        allPayInfo.setTradeStatus(0);// 交易状态0初始化1成功2失败
        allPayInfo.setAuditStatus(0); // 审核状态 0: 无需审核

        allPayInfo.setTransationOrderNo(request.getTransationOrderNo());
        allPayInfo.setSaleOrderNo(request.getSaleOrderNo());
        allPayInfo.setPnr(request.getPnr());
        return allPayInfo;
    }




}
