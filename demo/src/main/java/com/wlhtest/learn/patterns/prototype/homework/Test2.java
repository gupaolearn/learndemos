package com.wlhtest.learn.patterns.prototype.homework;

/**
 * @Author: wlh
 * @Date: 2019/3/13 16:07
 * @Version 1.0
 * @despricate:learn
 */
public class Test2 implements Cloneable {

    private int id;

    private static int i = 1;

    public Test2() {
        System.out.println("i = " + i);
        System.out.println("执行了构造函数"+i);
        i++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public static void main(String[] args) throws  Exception{
        Test2 t1 = new Test2();
        t1.setId(1);

        Test2 t2 = (Test2)t1.clone();

        System.out.println("t1 id: "+ t1.getId());
        System.out.println("t2 id: "+ t2.getId());
        System.out.println("t1 == t2 ? " + (t1 == t2));
        System.out.println("t1Class == t2Class ? " + (t1.getClass() == t2.getClass()));
        System.out.println("t1.equals(t2) ? " + t1.equals(t2));

        System.out.println("-------------------------------------------------");
        // after set new
        t1.setId(1000);
        System.out.println("t1 id: "+ t1.getId());
        System.out.println("t2 id: "+ t2.getId());
        System.out.println("t1 == t2 ? " + (t1 == t2));
        System.out.println("t1Class == t2Class ? " + (t1.getClass() == t2.getClass()));
        System.out.println("t1.equals(t2) ? " + t1.equals(t2));

    }


}
