package com.wlhtest.learn.patterns.template.jdbc;

/**
 * @Author: wlh
 * @Date: 2019/3/19 15:33
 * @Version 1.0
 * @despricate:learn
 */
public class User {

    private  String  name ;
    private  String password ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
