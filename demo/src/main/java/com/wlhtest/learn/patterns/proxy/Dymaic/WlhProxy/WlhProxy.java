package com.wlhtest.learn.patterns.proxy.Dymaic.WlhProxy;



import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;


public class WlhProxy   {

    public  static  final  String ln="\r\n";

    public static Object newProxyInstance(WlhClassLoader loader,
                                          Class<?>[] interfaces,
                                          WlhInvokeactionHandler h){


        try{
            //  生成代码
            String src=generateStr(interfaces);
           // System.out.println(src);
          //  输出到磁盘
            String filepath=WlhProxy.class.getResource("").getPath();
            File  f=new File(filepath+"$Proxy0.java");
            FileWriter writer=new FileWriter(f);
            writer.write(src);
            writer.flush();
            writer.close();

            //  手动编译新生成的文件
            JavaCompiler compiler= ToolProvider.getSystemJavaCompiler();
           StandardJavaFileManager  manager=compiler.getStandardFileManager(null,null,null);
            Iterable iterable=manager.getJavaFileObjects(f);
            JavaCompiler.CompilationTask task=compiler.getTask(null,manager,null,null,null,iterable);
            task.call();
            manager.close();

            Class proxyClass=loader.findClass("$Proxy0");
            Constructor  c=proxyClass.getConstructor(WlhInvokeactionHandler.class);
            return  c.newInstance(h);
        }catch (Exception e){
             e.printStackTrace();
        }

        return  null ;
    }

    //
    private static String generateStr(Class<?>[] interfaces) {

        // 字符串拼凑代码
        //1  引入相关的jar 包
        StringBuffer sb=new StringBuffer();
        sb.append("package com.wlhtest.learn.patterns.proxy.Dymaic.WlhProxy;"+ln);
        sb.append("import com.wlhtest.learn.patterns.proxy.Person;"+ln);
        sb.append("import java.lang.reflect.*  ;"+ln);

        // 2 创建新类的源码
        sb.append("public class $Proxy0  implements "+interfaces[0].getName()+" {" +ln);
        sb.append("WlhInvokeactionHandler h;"+ln);
        //  构造方法初始化参数
        sb.append("public $Proxy0(WlhInvokeactionHandler  h){"+ ln);
        sb.append(" this.h=h ;"+ln);
        sb.append(" }"+ln);

        //  生成对应的方法
        for (Method m:interfaces[0].getMethods())
        {
            sb.append("public  "+m.getReturnType().getName() +"  "+ m.getName() +" (){"+ln);
            sb.append("try{  "+ln);
               sb.append("  Method m="+interfaces[0].getName()+".class.getMethod("+"\""+m.getName()+"\",new Class[]{});"+ln);
               sb.append("this.h.invoke(this,m,null);"+ln);
               sb.append("} catch(Throwable e){"+ ln);
            sb.append(" e.printStackTrace();"+ln);
            sb.append(" }  "+ln) ;
            sb.append(" }"+ln);
        }

        sb.append("  }"+ln);



      return  sb.toString();

    }

}

