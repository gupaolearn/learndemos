package com.wlhtest.learn.patterns.factory.FactoryMethod;

/**
 * @Author: wlh
 * @Date: 2019/3/7 9:42
 * @Version 1.0
 * @despricate: 工厂模式的实现
 */
public interface IFactory {

    public ICar createCar();
}
