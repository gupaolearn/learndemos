/**
 * PayRequest.java
 * com.tempus.gss.pay.entity.vo
 *
 * Function： 支付请求 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2016-10-12 		amwu
 *
 * Copyright (c) 2016, TNT All Rights Reserved.
*/
package com.wlhtest.learn.patterns.strategy.homework;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * <p>
 * ClassName:PayRequest
 * </p>
 * <p>
 * Function:在线支付请求
 * </p>
 *
 * @author amwu
 * @version Ver 1.1
 * @since Ver 1.1
 * @date 2016-10-12 17:09:31
 * @see
 */
@SuppressWarnings("serial")
public class OnLinePayRequest implements Serializable {

	/** 交易方式code **/
	private Integer payWayCode;

	/** 渠道 B2B,B2C **/
	private String channel;

	/** 业务线 1-支付 2-充值, 此字段暂时不用 **/
	private String serviceLine;

	/** 客户Id **/
	private Long cusId;

	/** 订单号 **/
	private Long orderNo;

	/** 交易金额 **/
	private BigDecimal balance;

	/** 客户额度账号，额度支付必填 **/
	private Long AccoutNo;

	/** 客户备注 **/
	private String custRemark;

	/** 返回地址 第三方支付必填 **/
	private String returnUrl;

	/** 交易单号 */
	private String transationOrderNo;

	/** 销售单号 */
	private String saleOrderNo;

	/** pnr */
	private String pnr;

	
	

	/**
	 * 获取交易方式code
	 *
	 * @return payWayCode 交易方式code
	 */
	public Integer getPayWayCode() {

		return payWayCode;
	}

	/**
	 * 设置交易方式code
	 *
	 * @param payWayCode
	 *            交易方式code
	 */
	public void setPayWayCode(Integer payWayCode) {

		this.payWayCode = payWayCode;
	}

	/**
	 * 获取渠道
	 *
	 * @return channel 渠道
	 */
	public String getChannel() {

		return channel;
	}

	/**
	 * 设置渠道
	 *
	 * @param channel
	 *            渠道
	 */
	public void setChannel(String channel) {

		this.channel = channel;
	}

	/**
	 * 获取业务线
	 *
	 * @return serviceLine 业务线
	 */
	public String getServiceLine() {

		return serviceLine;
	}

	/**
	 * 设置业务线
	 *
	 * @param serviceLine
	 *            业务线
	 */
	public void setServiceLine(String serviceLine) {

		this.serviceLine = serviceLine;
	}

	/**
	 * 获取客户Id
	 *
	 * @return cusId 客户Id
	 */
	public Long getCusId() {

		return cusId;
	}

	/**
	 * 设置客户Id
	 *
	 * @param cusId
	 *            客户Id
	 */
	public void setCusId(Long cusId) {

		this.cusId = cusId;
	}

	/**
	 * 获取订单号
	 *
	 * @return orderNo 订单号
	 */
	public Long getOrderNo() {

		return orderNo;
	}

	/**
	 * 设置订单号
	 *
	 * @param orderNo
	 *            订单号
	 */
	public void setOrderNo(Long orderNo) {

		this.orderNo = orderNo;
	}

	/**
	 * 获取交易金额
	 *
	 * @return balance 交易金额
	 */
	public BigDecimal getBalance() {

		return balance;
	}

	/**
	 * 设置交易金额
	 *
	 * @param balance
	 *            交易金额
	 */
	public void setBalance(BigDecimal balance) {

		this.balance = balance;
	}

	/**
	 * 获取accoutNo
	 *
	 * @return accoutNo accoutNo
	 */
	public Long getAccoutNo() {

		return AccoutNo;
	}

	/**
	 * 设置accoutNo
	 *
	 * @param accoutNo
	 *            accoutNo
	 */
	public void setAccoutNo(Long accoutNo) {

		AccoutNo = accoutNo;
	}

	/**
	 * 获取客户备注
	 *
	 * @return custRemark 客户备注
	 */
	public String getCustRemark() {

		return custRemark;
	}

	/**
	 * 设置客户备注
	 *
	 * @param custRemark
	 *            客户备注
	 */
	public void setCustRemark(String custRemark) {

		this.custRemark = custRemark;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getTransationOrderNo() {
		return transationOrderNo;
	}

	public void setTransationOrderNo(String transationOrderNo) {
		this.transationOrderNo = transationOrderNo;
	}

	public String getSaleOrderNo() {
		return saleOrderNo;
	}

	public void setSaleOrderNo(String saleOrderNo) {
		this.saleOrderNo = saleOrderNo;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}


	@Override
	public String toString() {
		return "OnLinePayRequest [payWayCode=" + payWayCode + ", channel=" + channel + ", serviceLine=" + serviceLine
				+ ", cusId=" + cusId + ", orderNo=" + orderNo + ", balance=" + balance + ", AccoutNo=" + AccoutNo
				+ ", custRemark=" + custRemark + ", returnUrl=" + returnUrl + ", transationOrderNo=" + transationOrderNo
				+ ", saleOrderNo=" + saleOrderNo + ", pnr=" + pnr + ", ProductType="  + ", payWay="
				 + "]";
	}
}
