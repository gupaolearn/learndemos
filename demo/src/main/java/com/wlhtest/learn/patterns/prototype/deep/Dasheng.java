package com.wlhtest.learn.patterns.prototype.deep;

import java.io.*;
import java.util.Date;

/**
 * @Author: wlh
 * @Date: 2019/3/13 13:58
 * @Version 1.0
 * @despricate:learn
 */
public class Dasheng extends  Monkey  implements Serializable,Cloneable {

   public  JinguBang jinguBang ;

   public  Dasheng(){

       this.birthday=new Date();
       this.jinguBang=new JinguBang();

   }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return  deepClone();
    }

    public Object deepClone(){

       try{
           ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
           ObjectOutputStream objectOutputStream=new ObjectOutputStream(byteArrayOutputStream);
           objectOutputStream.writeObject(this);

           ByteArrayInputStream byteArrayInputStream=new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
           ObjectInputStream objectInputStream=new ObjectInputStream(byteArrayInputStream);

           Dasheng dasheng=(Dasheng)objectInputStream.readObject();
           dasheng.birthday=new Date();

           return  dasheng ;

       }catch (Exception e){
           e.printStackTrace();
       }
       return  null ;
    }
}
