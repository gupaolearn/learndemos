package com.wlhtest.learn.patterns.template.jdbc;

import org.apache.ibatis.datasource.pooled.PooledDataSource;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.List;
import java.util.logging.Logger;

/**
 * @Author: wlh
 * @Date: 2019/3/19 16:03
 * @Version 1.0
 * @despricate:learn
 */
public class JdbcTest {

    public static void main(String[] args) throws Exception{
        DataSource dataSource=new PooledDataSource("com.mysql.jdbc.Driver",
                "jdbc:mysql://127.0.0.1/test?useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true&autoReconnect=true",
                "root","root");
        UserDao userDao=new UserDao(dataSource);
        List<?> res=userDao.selectAll();
        if(res!=null&&res.size()>0){
            for(int i=0 ;i<res.size();i++)
            {
                System.out.println(res.get(i).toString());
            }
        }
    }
}
