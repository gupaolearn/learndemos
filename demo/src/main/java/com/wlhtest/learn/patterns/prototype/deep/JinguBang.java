package com.wlhtest.learn.patterns.prototype.deep;

import java.io.Serializable;

/**
 * @Author: wlh
 * @Date: 2019/3/13 13:55
 * @Version 1.0
 * @despricate:learn
 */

public class JinguBang implements Serializable {

    public  float height=100 ;
    public  float weight=100 ;

    public  void  Big(){
        this.height*=2 ;
        this.weight*=2 ;
    }


    public  void  Small()
    {
        this.height/=2 ;
        this.weight/=2 ;
    }

}
