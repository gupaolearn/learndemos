package com.wlhtest.learn.patterns.factory.AbstrFactory;

/**
 * @Author: wlh
 * @Date: 2019/3/8 17:32
 * @Version 1.0
 * @despricate:learn
 */
public  interface AbstrCar {

    void  start();

}
