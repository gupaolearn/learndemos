package com.wlhtest.learn.patterns.template.jdbc;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;

/**
 * @Author: wlh
 * @Date: 2019/3/19 15:50
 * @Version 1.0
 * @despricate:learn
 */
public class UserDao extends  JdbcTemplate {


    public UserDao(DataSource dataSource) {
        super(dataSource);
    }

    public List<?>  selectAll() throws  Exception{
        String sql="select  *  from  user1";
        RowMapper rowMapper=new RowMapper() {
            public User rowToMap(ResultSet resultSet, int rowNum) throws Exception {
                    User user=new User() ;
                    user.setName(resultSet.getString("name"));
                    user.setPassword(resultSet.getString("password"));
                return user;
            }
        };
      return super.selectAll(sql, rowMapper,null);
    }
}
