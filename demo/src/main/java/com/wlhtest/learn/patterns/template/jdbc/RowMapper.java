package com.wlhtest.learn.patterns.template.jdbc;

import java.sql.ResultSet;

/**
 * @Author: wlh
 * @Date: 2019/3/19 15:12
 * @Version 1.0
 * @despricate:简单实现jdbc功能中的将查询出来的数据映射为实体类
 */
public interface RowMapper<T> {

    T rowToMap(ResultSet resultSet,int rowNum) throws  Exception;

}
