package com.wlhtest.learn.patterns.prototype.deep;

/**
 * @Author: wlh
 * @Date: 2019/3/13 14:07
 * @Version 1.0
 * @despricate:learn
 */
public class DeepCloneTest {

    public static void main(String[] args) {

        // 深度克隆： 相当于重新new 了对象， 所以两者的引用地址是不相等的
        //  如果克隆对象是单例对象，那么深克隆会破坏单例。
        // 防止克隆破坏单例有：1 禁止克隆，即不实现clone接口；2  重写clone方法，直接返回单例对象即可

        Dasheng dasheng=new Dasheng() ;
        try{
            Dasheng clone1=(Dasheng) dasheng.clone() ;
            System.out.println(dasheng==clone1);
            System.out.println(dasheng.jinguBang==clone1.jinguBang);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
