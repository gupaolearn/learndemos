package com.wlhtest.learn.patterns.singleton.lazy;

public class LazySingleton {


    private  static  LazySingleton lazySingleton=null;
    private  LazySingleton(){

    }

    //  如果不加 synchronized 会存在线程安全问题 即两个线程同时进入了 15 行 代码的执行  加入synchronized 后只有一个线程能进入
    //  如果另外一个线程获得cpu 的执行权时 ，但是没法获取LazySingleton 的类锁，也就不能执行此方法 ， 也就避免了两个线程同时进入的情况
    // jdk1.6 之后对LazySingleton的性能优化了不少   但是不可避免的还是存在一定的性能问题
    public   synchronized static   LazySingleton getInstance(){
        if (lazySingleton==null){
            lazySingleton=new LazySingleton() ;
        }
        return  lazySingleton;
    }
}
