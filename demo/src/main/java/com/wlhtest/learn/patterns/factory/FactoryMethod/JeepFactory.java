package com.wlhtest.learn.patterns.factory.FactoryMethod;

/**
 * @Author: wlh
 * @Date: 2019/3/7 9:52
 * @Version 1.0
 * @despricate:learn
 */
public class JeepFactory implements  IFactory {
    @Override
    public ICar createCar() {
        return new Jeep();
    }
}
