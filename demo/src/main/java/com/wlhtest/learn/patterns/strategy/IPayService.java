package com.wlhtest.learn.patterns.strategy;

/**
 * @Author: wlh
 * @Date: 2019/3/18 15:12
 * @Version 1.0
 * @despricate:learn
 */
public interface IPayService {

    void  doPay();
}
