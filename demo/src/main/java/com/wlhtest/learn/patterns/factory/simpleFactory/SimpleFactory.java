package com.wlhtest.learn.patterns.factory.simpleFactory;

public class SimpleFactory {

    /*
    *  简单工厂模式的优缺点：
    *  优点：简单工厂模式能够根据外界给定的信息决定具体创建哪个类的对象，明确区分了各自的职责和权利
    *  缺点：集中了所有类的对象创建逻辑，违反了开闭原则、高内聚低耦合的原则
    * */


    public ICoures createCoures(String couresName){
        if(null==couresName||"".equals(couresName))
        return  null ;
        if("java".equals(couresName))
            return  new JavaCoures();
        else if ("python".equals(couresName))
            return  new PythonCoures();
        else
            return  null ;

    }
}
