package com.wlhtest.learn.patterns.proxy.Dymaic.WlhProxy;


import com.wlhtest.learn.patterns.proxy.Person;

import java.lang.reflect.Method;

public class WlhMeipo  implements  WlhInvokeactionHandler{

    private Person target ;


    public Object getInstance(Person person) throws  Exception{
        this.target=person;
        Class<?> clazz=target.getClass();
        return WlhProxy.newProxyInstance(new WlhClassLoader(),clazz.getInterfaces(),this);
    }


    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object object= method.invoke(this.target,args);
        after();
        return  object ;
    }


    private void  before(){
        System.out.println("代理前置检查");
    }

    private  void  after(){
        System.out.println("代理后置处理");
    }
}
