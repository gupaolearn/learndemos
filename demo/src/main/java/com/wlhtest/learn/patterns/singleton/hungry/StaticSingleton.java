package com.wlhtest.learn.patterns.singleton.hungry;

public class StaticSingleton {

    //  饿汉单例模式 实现
    /*public static  final  HungrySingleton hungruSingleton=new HungrySingleton();
    private HungrySingleton(){

    }
    public static  HungrySingleton getInstance(){
     return    hungruSingleton ;
    }*/
    //  其效果类似以下的代码：  在类加载的时候就实例化
    //  缺点： 浪费内存空间
    public static  final StaticSingleton staticSingleton;

    static {
        staticSingleton=new StaticSingleton();
    }


    private StaticSingleton(){

    }
    public static StaticSingleton getInstance(){
        return    staticSingleton ;
    }
}
