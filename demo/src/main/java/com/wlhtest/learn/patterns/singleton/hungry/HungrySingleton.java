package com.wlhtest.learn.patterns.singleton.hungry;

public class HungrySingleton {

    //  饿汉单例模式 实现
    /*public static  final  HungrySingleton hungruSingleton=new HungrySingleton();
    private HungrySingleton(){

    }
    public static  HungrySingleton getInstance(){
     return    hungruSingleton ;
    }*/
    //  其效果类似以下的代码：  在类加载的时候就实例化
    //  缺点： 浪费内存空间
    public static  final  HungrySingleton hungruSingleton;

    static {
        hungruSingleton=new HungrySingleton();
    }


    private HungrySingleton(){

    }
    public static  HungrySingleton getInstance(){
        return    hungruSingleton ;
    }
}
