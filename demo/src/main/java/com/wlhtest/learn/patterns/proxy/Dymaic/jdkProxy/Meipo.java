package com.wlhtest.learn.patterns.proxy.Dymaic.jdkProxy;

import com.wlhtest.learn.patterns.proxy.Person;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Meipo  implements InvocationHandler {


    private Person target ;


    public Meipo(){

    }

    public Object getInstance(Person person) throws  Exception{
        this.target=person;
        Class<?> clazz=target.getClass();
        return Proxy.newProxyInstance(clazz.getClassLoader(),clazz.getInterfaces(),this);
    }


    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object object= method.invoke(this.target,args);
        after();
        return  object ;
    }


    private void  before(){
        System.out.println("代理前置检查");
    }

    private  void  after(){
        System.out.println("代理后置处理");
    }

}
